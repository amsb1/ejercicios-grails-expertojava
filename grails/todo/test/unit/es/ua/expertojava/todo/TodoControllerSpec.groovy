package es.ua.expertojava.todo

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(TodoController)
@Mock([Todo, Category])
class TodoControllerSpec extends Specification {

    def todoService = Mock(TodoService)
    def springSecurityService = Mock(SpringSecurityService)
    def user
    def testusr1, testusr2


    def setup() {
        def todoPaintKitchen = new Todo(title: "Pintar cocina", done: true, date: new Date() + 1, user: testusr1).save(flush:true)
        def todoCollectPost = new Todo(title: "Recoger correo postal", done: false, date: new Date() + 2, user: testusr1).save(flush:true)
        user = new User(username: "testusr1", password: "testusr1", confirmPassword: "testusr1", name: "testusr1", surnames: "none", email: "testusr1@test.es").save();

        controller.todoService = todoService
        todoService.getTodosByCategoryAndUserOrderByDate() >> [todoPaintKitchen,todoCollectPost]
        todoService.listByUser() >> []
        controller.springSecurityService = springSecurityService
        springSecurityService.getCurrentUser() >> user
        springSecurityService.isLoggedIn() >> true
        testusr1 = new User(username: "testusr1", password: "testusr1", confirmPassword: "testusr1", name: "testusr1", surnames: "none", email: "testusr1@test.es").save();
        testusr2 = new User(username: "testusr2", password: "testusr2", confirmPassword: "testusr2", name: "testusr2", surnames: "none", email: "testusr2@test.es").save();
    }

    def populateValidParams(params) {
        assert params != null

        params["title"] = 'title'
        params["completed"] = false
        params["date"] = new Date()
        params["user"] = testusr1
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
        controller.index()

        then:"The model is correct"
        !model.todoInstanceList
        model.todoInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
        controller.create()

        then:"The model is correctly created"
        model.todoInstance!= null
    }

   /* void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        def todo = new Todo()
        todo.validate()
        controller.save(todo)

        then:"The create view is rendered again with the correct model"
        model.todoInstance!= null
        view == 'create'

        when:"The save action is executed with a valid instance"
        response.reset()
        populateValidParams(params)
        todo = new Todo(params)

        controller.save(todo)

        then:"A redirect is issued to the show action"
        response.redirectedUrl == '/todo/show/1'
        controller.flash.message != null
        Todo.count() == 1
    }*/

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
        controller.show(null)

        then:"A 404 error is returned"
        response.status == 404

        when:"A domain instance is passed to the show action"
        populateValidParams(params)
        def todo = new Todo(params)
        controller.show(todo)

        then:"A model is populated containing the domain instance"
        model.todoInstance == todo
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
        controller.edit(null)

        then:"A 404 error is returned"
        response.status == 404

        when:"A domain instance is passed to the edit action"
        populateValidParams(params)
        def todo = new Todo(params)
        controller.edit(todo)

        then:"A model is populated containing the domain instance"
        model.todoInstance == todo
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'DELETE'
        controller.delete(null)

        then:"A 404 is returned"
        response.redirectedUrl == '/todo/index'
        flash.message != null

        when:"A domain instance is created"
        response.reset()
        populateValidParams(params)
        def todo = new Todo(params).save(flush: true)

        then:"It exists"
        Todo.count() == 1

        when:"The domain instance is passed to the delete action"
        controller.delete(todo)

        then:"The instance is deleted"
        Todo.count() == 1
        response.redirectedUrl == '/todo/index'
        flash.message != null
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'PUT'
        controller.update(null)

        then:"A 404 error is returned"
        response.redirectedUrl == '/todo/index'
        flash.message != null


        when:"An invalid domain instance is passed to the update action"
        response.reset()
        def todo = new Todo()
        todo.validate()
        controller.update(todo)

        then:"The edit view is rendered again with the invalid instance"
        view == 'edit'
        model.todoInstance == todo

        when:"A valid domain instance is passed to the update action"
        response.reset()
        populateValidParams(params)
        todo = new Todo(params).save(flush: true)
        controller.update(todo)

        then:"A redirect is issues to the show action"
        response.redirectedUrl == "/todo/show/$todo.id"
        flash.message != null
    }

    // Tests T6 punto 2
    def "ComprobamosListNextTodos"() {
        given:
        def todoPaintKitchen = new Todo(title: "Pintar cocina", done: true, date: new Date() + 1, user: testusr1).save(flush:true)
        def todoCollectPost = new Todo(title: "Recoger correo postal", done: false, date: new Date() + 2, user: testusr1).save(flush:true)
        def todoBakeCake = new Todo(title: "Cocinar pastel", done: true, date: new Date() + 4, user: testusr2).save(flush:true)
        def todoWriteUnitTests = new Todo(title: "Escribir tests unitarios", done: false, date: new Date(), user: testusr2).save(flush:true)
        when:
        controller.listNextTodos(2)
        then:
        [todoPaintKitchen,todoCollectPost]
    }

    def "ComprobamosSelectCategories"() {
        given:
        def categoryHome = new Category(name: "Hogar").save(flush: true)
        def categoryJob = new Category(name: "Trabajo").save(flush: true)
        when:
        controller.selectCategories()
        then: ""
        and:
        model.categoryInstanceList.containsAll([categoryHome, categoryJob])
    }

}
