package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Category)
class CategorySpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    // Test con 'description'
    def "DescripcionPuedeSerVacia"() {
        given:
        def c1 = new Category(description: "")
        when:
        c1.validate()
        then:
        !c1?.errors['description']
    }

    def "DescripcionPuedeSerNull"() {
        given:
        def c1 = new Category(description: null)
        when:
        c1.validate()
        then:
        !c1?.errors['description']
    }

    @Unroll
    def "DescripcionDebeTenerMenosDe1001"() {
        given:
        def c1 = new Category(description: "a"*characters)
        when:
        c1.validate()
        then:
        !c1?.errors['description']
        where:
        characters << [0,1,999,1000]
    }

    @Unroll
    def "ErrorSiDescripcionMayorDe1001"() {
        given:
        def c1 = new Category(description: "-"*characters)
        when:
        c1.validate()
        then:
        c1?.errors['description']
        where:
        characters << [1002]
    }

    // Test con 'name'
    def "NombreNoVacio"() {
        given:
        def c1 = new Category(name:"")
        when:
        c1.validate()
        then:
        c1?.errors['name']
    }

    def "SiNombreNoVacioNoError"() {
        given:
        def c1 = new Category(name:"name")
        when:
        c1.validate()
        then:
        !c1?.errors['name']
    }
}
