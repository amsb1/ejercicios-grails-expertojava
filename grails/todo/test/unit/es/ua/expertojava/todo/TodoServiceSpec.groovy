package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TodoService)
class TodoServiceSpec extends Specification {

    def testusr1, testusr2

    def setup() {
        testusr1 = new User(username: "testusr1", password: "testusr1", confirmPassword: "testusr1", name: "testusr1", surnames: "none", email: "testusr1@test.es");
        testusr2 = new User(username: "testusr2", password: "testusr2", confirmPassword: "testusr2", name: "testusr2", surnames: "none", email: "testusr2@test.es");
    }

    def cleanup() {
    }

    // Tests T6 punto 3
    def "MetodoCountNextDevuelveNumeroPendientesPasadoPorParametro"() {
        given:
        def todoTest1 = new Todo(title: "Test1", done: true, date: new Date() + 1, user: testusr1)
        def todoTest2 = new Todo(title: "Test2", done: false, date: new Date() + 2, user: testusr1)
        def todoTest3 = new Todo(title: "Test3", done: true, date: new Date() + 4, user: testusr2)
        def todoTest4 = new Todo(title: "Test4", done: false, date: new Date(), user: testusr2)
        and:
        mockDomain(Todo,[todoTest1,todoTest2,todoTest3,todoTest4])
        when:
        def countNodos = service.countNext(2)
        then:
        Todo.count() == 4
        and:
        countNodos == 2
    }

    def "MetodoGetNextDevuelveNumeroPendientesPasadoPorParametro"() {
        given:
        def todoTest1 = new Todo(title: "Test1", done: true, date: new Date() + 1, user: testusr1)
        def todoTest2 = new Todo(title: "Test2", done: false, date: new Date() + 2, user: testusr1)
        def todoTest3 = new Todo(title: "Test3", done: true, date: new Date() + 4, user: testusr2)
        def todoTest4 = new Todo(title: "Test4", done: false, date: new Date(), user: testusr2)
        and:
        mockDomain(Todo,[todoTest1,todoTest2,todoTest3,todoTest4])
        when:
        def countNodos = service.getNext(2,[:])
        then:
        Todo.count() == 4
        and:
        countNodos.size() == 2
    }

    void "MetodoSaveTodoTieneFechaIgualFechaCompletada"() {
        given:
        def todoTest4 = new Todo(title: "Test4", done: false, date: new Date(), user: testusr2)
        expect:
        todoTest4.done == false
        todoTest4.dateDone == null
        when:
        todoTest4.done = true
        and:
        service.saveTodo(todoTest4)
        then:
        todoTest4.dateDone != null
        and:
        todoTest4.dateDone instanceof Date
    }

    void "MetodoSaveTodoNoActualizaFechaRealizaciónSiTareaNoCompletada"() {
        given:
        def todoTest4 = new Todo(title: "Test4", done: false, date: new Date(), user: testusr2)
        expect:
        todoTest4.done == false
        todoTest4.dateDone == null
        when:
        todoTest4.done = false
        and:
        service.saveTodo(todoTest4)
        then:
        todoTest4.dateDone == null
    }

    void "MetodoSaveTodoGuardaInstancias"(){
        given:
        def todo = new Todo(title:"TodoTest", date: new Date(), done:true)
        and:
        mockDomain(Todo,[todo])
        and:
        todo.dateDone == null
        and:
        service.saveTodo(todo)
        expect:
        todo.dateDone != null
        todo.title.equals("TodoTest")
    }

}
