package es.ua.expertojava.todo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def testusr

    def setup() {
        testusr = new User(username: "usuario1", password: "usuario1", confirmPassword: "usuario1", name: "testusr", surnames: "none", email: "testusr@test.es").save();
    }

    def cleanup() {
    }

    @Unroll
    void "ErrorSiFechaRecordatorioNuncaPosteriorFechaTarea"() {
        given:
        def t1 = new Todo(title: "Test TODO", done: false, date: new Date(), reminderDate: new Date() + 1, user: testusr)
        when:
        t1.validate()
        then:
        t1?.errors['reminderDate']
    }

    @Unroll
    void "NoErrorSiFechaRecordatorioAnteriorFechaTarea"() {
        given:
        def t1 = new Todo(title: "Test TODO2", done: false, date: new Date(), reminderDate: new Date() - 1, user: testusr)
        when:
        t1.validate()
        then:
        !t1?.errors['reminderDate']
    }
}
