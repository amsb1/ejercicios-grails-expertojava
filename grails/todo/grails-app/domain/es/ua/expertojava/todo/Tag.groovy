package es.ua.expertojava.todo

class Tag {
    String name
    String colorRGB = "#212121"

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false, nullable:true, unique:true)
        colorRGB shared:"rgbcolor"
    }

    String toString(){
        name
    }
}
