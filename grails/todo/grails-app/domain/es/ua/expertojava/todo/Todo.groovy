package es.ua.expertojava.todo

class Todo {
    String title
    String description
    Date date
    Date reminderDate
    String url
    Boolean done=false

    Category category

    User user

    Date dateCreated
    Date lastUpdated

    Date dateDone

    static hasMany = [tags:Tag]
    static belongsTo = [Tag,User]

    static mapping = {
        autoTimestamp true
    }

    static searchable = true

    static constraints = {
        title(blank:false)
        description(blank:true, nullable:true, maxSize:1000)
        date(nullable:false)
        reminderDate(nullable:true, validator: {val, obj ->
            if(val && obj?.date){
                return val.before(obj.date)
            }
            return true
        })
        url(nullable:true, url:true)
        done(nullable:false, blank:false)
        category(nullable:true)
        dateDone(nullable:true)
        dateCreated(nullable: true)
        lastUpdated(nullable: true)
    }

    String toString(){
        title
    }
}
