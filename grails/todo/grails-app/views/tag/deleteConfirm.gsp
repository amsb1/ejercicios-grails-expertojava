<%--
  Created by IntelliJ IDEA.
  User: expertojava
  Date: 19/02/16
  Time: 15:21
--%>

<%@ page import="es.ua.expertojava.todo.Tag" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<div id="show-tag" class="content scaffold-show" role="main">
    <div style="text-align: center">
        <p>
            <img src="/todo/assets/warning.png" style="width: 100px">
            <h5>Confirma el borrado de la etiqueta?</h5>
        </p>
    </div>

    <g:form url="[resource:tagInstance, action:'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Confirm')}" />
        </fieldset>
    </g:form>
</div>
</body>
</html>