package es.ua.expertojava.todo

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import es.ua.expertojava.todo.*

@Transactional(readOnly = true)

class CategoryController {
    def springSecurityService
    def CategoryService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Category - Accion index - Modelo []")
        params.max = Math.min(max ?: 10, 100)
        respond Category.list(params), model:[categoryInstanceCount: Category.count()]
    }

    def show(Category categoryInstance) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Category - Accion show - Modelo []")
        respond categoryInstance
    }

    def create() {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Category - Accion create - Modelo []")
        respond new es.ua.expertojava.todo.Category(params)
    }

    @Transactional
    def save(Category categoryInstance) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Category - Accion save - Modelo []")
        if (categoryInstance == null) {
            notFound()
            return
        }

        if (categoryInstance.hasErrors()) {
            respond categoryInstance.errors, view:'create'
            return
        }

        categoryInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'category.label', default: 'Category'), /*categoryInstance.id*/ "'"+categoryInstance.name+"'"])
                redirect categoryInstance
            }
            '*' { respond categoryInstance, [status: CREATED] }
        }
    }

    def edit(Category categoryInstance) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Category - Accion edit - Modelo []")
        respond categoryInstance
    }

   // @Transactional
    def update(Category categoryInstance) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Category - Accion update - Modelo []")
        if (categoryInstance == null) {
            notFound()
            return
        }

        if (categoryInstance.hasErrors()) {
            respond categoryInstance.errors, view:'edit'
            return
        }

        CategoryService.delete(categoryInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Category.label', default: 'Category'), categoryInstance.id])
                redirect categoryInstance
            }
            '*'{ respond categoryInstance, [status: OK] }
        }
    }

   // @Transactional
    def deleteCategory(Category categoryInstance) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Category - Accion deleteCategory - Modelo [${categoryInstance}]")

        if (categoryInstance == null) {
            notFound()
            return
        }

        categoryInstance.delete(flush:true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Category.label', default: 'Category'), categoryInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'category.label', default: 'Category'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
