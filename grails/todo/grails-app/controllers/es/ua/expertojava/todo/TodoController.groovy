package es.ua.expertojava.todo

import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TodoController {

    def springSecurityService
    def todoService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null
        log.trace("User ${user.username} - Controlador Todo - Accion index - Modelo []")
        /*params.max = Math.min(max ?: 10, 100)

        respond todoService.listByUser(springSecurityService.getCurrentUser(),params), model:[todoInstanceCount:0]*/
        params.max = Math.min(max ?: 10, 100)



        if(user.getUsername()=="admin") {
            respond Todo.list(params), model:[todoInstanceCount: Todo.count()]
        }
        else {
            def listaAux = []
            listaAux = Todo.findAllByUser(user)
            if (listaAux == null) listaAux = []

            respond listaAux,
                    model:[todoInstanceCount: listaAux.size()],
                    view: "index"
        }
    }

    def create() {
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Todo - Accion create - Modelo []")
        respond new Todo(params)
    }

    def show(Todo todoInstance) {
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Todo - Accion show - Modelo []")
        respond todoInstance
    }

    def edit(Todo todoInstance) {
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Todo - Accion edit - Modelo []")
        respond todoInstance
    }

    def showTodosByUser(){
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Todo - Accion listNextTodos - Modelo []")
        params.max = Math.min(max ?: 10, 100)
        respond todoService.showTodosByUser(params.username,max)
    }

    def listNextTodos(Integer days) {
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Todo - Accion listNextTodos - Modelo []")
        respond todoService.getNext(days, params),
                model: [todoInstanceCount: todoService.countNext(days)],
                view: "index"
    }

    def listByCategory() {
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Todo - Accion listByCategory - Modelo []")

        def categorias = []

        params?.category?.each { id ->
            categorias.add(Category.get(id))
        }

        respond todoService.getTodosByCategoryOrderByDate(categorias),
                model: [todoInstanceCount: todoService.countTodosByCategory(categorias)]
    }

    def selectCategories() {
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Todo - Accion selectCategories - Modelo []")
        render model: [categoryInstanceList: Category.list()],
                view: 'selectCategories'
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Transactional
    def save(Todo todoInstance) {
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Todo - Accion save - Modelo []")
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create'
            return
        }

        //todoInstance.save flush:true
        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), "'"+todoInstance.title+"'"])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }

    @Transactional
    def update(Todo todoInstance) {
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Todo - Accion update - Modelo []")
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'edit'
            return
        }

        //todoInstance.save flush:true
        todoService.saveTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {
        User user = springSecurityService.isLoggedIn() ? springSecurityService.getCurrentUser() : null
        log.trace("User ${user.username} - Controlador Todo - Accion delete - Modelo []")
        if (todoInstance == null) {
            notFound()
            return
        }

        //todoInstance.delete flush:true
        todoService.delete(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
}
