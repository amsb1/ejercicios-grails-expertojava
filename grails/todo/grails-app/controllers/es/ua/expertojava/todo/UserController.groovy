package es.ua.expertojava.todo

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserController {
    def springSecurityService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]


    def index(Integer max) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador User - Accion Index - Modelo []")
        params.max = Math.min(max ?: 10, 100)
        respond User.list(params), model:[userInstanceCount: User.count()]
    }

    def show(User userInstance) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador User - Accion Show - Modelo []")
        respond userInstance
    }

    def create() {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador User - Accion Create - Modelo []")
        respond new User(params)
    }

    @Transactional
    def save(User userInstance) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador User - Accion Save - Modelo []")
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'create'
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), userInstance.id])
                redirect userInstance
            }
            '*' { respond userInstance, [status: CREATED] }
        }
    }

    def edit(User userInstance) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador User - Accion edit - Modelo []")
        respond userInstance
    }

    @Transactional
    def update(User userInstance) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador User - Accion update - Modelo []")
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'edit'
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect userInstance
            }
            '*'{ respond userInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(User userInstance) {
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador User - Accion delete - Modelo []")

        if (userInstance == null) {
            notFound()
            return
        }

        userInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
