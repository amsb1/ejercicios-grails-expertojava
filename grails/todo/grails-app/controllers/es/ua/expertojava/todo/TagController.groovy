package es.ua.expertojava.todo

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TagController {

    def springSecurityService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        //log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Tag - Accion index - Modelo []")
        params.max = Math.min(max ?: 10, 100)
        respond Tag.list(params), model:[tagInstanceCount: Tag.count()]
    }

    def show(Tag tagInstance) {
//        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Tag - Accion show - Modelo []")
        if (tagInstance == null) {
            notFound()
            return
        }
        respond tagInstance
    }

    def create() {
//        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Tag - Accion create - Modelo []")
        Tag tagInstance = new Tag(params)
        respond tagInstance
    }

    @Transactional
    def save(Tag tagInstance) {
//        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Tag - Accion save - Modelo []")
        if (tagInstance == null) {
            notFound()
            return
        }

        if (tagInstance.hasErrors()) {
            respond tagInstance.errors, view:'create'
            return
        }

        tagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tag.label', default: 'Tag'), "'"+tagInstance.name +"'"])
                redirect tagInstance
            }
            '*' { respond tagInstance, [status: CREATED] }
        }
    }

    def edit(Tag tagInstance) {
//        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Tag - Accion edit - Modelo []")
        if (tagInstance == null) {
            notFound()
            return
        }
        respond tagInstance
    }

    @Transactional
    def update(Tag tagInstance) {
//        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Tag - Accion update - Modelo []")
        if (tagInstance == null) {
            notFound()
            return
        }

        if (tagInstance.hasErrors()) {
            respond tagInstance.errors, view:'edit'
            return
        }

        tagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Tag.label', default: 'Tag'),  "'"+tagInstance.name +"'"])
                redirect tagInstance
            }
            '*'{ respond tagInstance, [status: OK]}
        }
    }

    def deleteConfirm(Tag  tagInstance){
        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Tag - Accion deleteConfirm - Modelo []")
        respond tagInstance
    }

    @Transactional
    def delete(Tag tagInstance) {
//        log.trace("User ${springSecurityService.getCurrentUser()} - Controlador Tag - Accion delete - Modelo []")
        if (tagInstance == null) {
            notFound()
            return
        }

        tagInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Tag.label', default: 'Tag'), tagInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tag.label', default: 'Tag'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

