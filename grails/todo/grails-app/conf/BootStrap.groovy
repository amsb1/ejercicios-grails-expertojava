import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
        try {
            // Usuarios y permisos
            def roleUsuario = new Role(authority:'ROLE_BASIC').save(flush: true)
            def roleAdmin = new Role(authority:'ROLE_ADMIN').save(flush: true)

            def userAdmin = new User(username:"admin", password:"admin", name:"admin",surnames:"NONE",confirmPassword:"admin",email:"admin@usuario.com").save(flush: true)
            def user1 = new User(username:"usuario1", password:"usuario1",name:"usuario1",surnames:"NONE",confirmPassword:"usuario1",email:"usuario1@usuario.com").save(flush: true)
            def user2 = new User(username:"usuario2", password:"usuario2",name:"usuario2",surnames:"NONE",confirmPassword:"usuario2",email:"usuario2@usuario.com").save(flush: true)

             PersonRole.create(userAdmin,roleAdmin).save()
             PersonRole.create(user1,roleUsuario).save()
             PersonRole.create(user2,roleUsuario).save()
            //-----------------------------------------------------------

            def categoryHome = new Category(name:"Hogar").save()
            def categoryJob = new Category(name:"Trabajo").save()

            def tagEasy = new Tag(name:"Fácil").save()
            def tagDifficult = new Tag(name:"Difícil").save()
            def tagArt = new Tag(name:"Arte").save()
            def tagRoutine = new Tag(name:"Rutina").save()
            def tagKitchen = new Tag(name:"Cocina").save()

            def todoPaintKitchen = new Todo(title:"Pintar cocina", date:new Date()+1,done:true)
            def todoCollectPost = new Todo(title:"Recoger correo postal", date:new Date()+2,done:false)
            def todoBakeCake = new Todo(title:"Cocinar pastel", date:new Date()+4,done:false)
            def todoWriteUnitTests = new Todo(title:"Escribir tests unitarios", date:new Date(),done:false)

            user1.addToTodos(todoPaintKitchen)
            user1.addToTodos(todoCollectPost)
            user2.addToTodos(todoBakeCake)
            user2.addToTodos(todoWriteUnitTests)

            todoPaintKitchen.addToTags(tagDifficult)
            todoPaintKitchen.addToTags(tagArt)
            todoPaintKitchen.addToTags(tagKitchen)
            todoPaintKitchen.category = categoryHome
            todoPaintKitchen.save()

            todoCollectPost.addToTags(tagRoutine)
            todoCollectPost.category = categoryJob
            todoCollectPost.save()

            todoBakeCake.addToTags(tagEasy)
            todoBakeCake.addToTags(tagKitchen)
            todoBakeCake.category = categoryHome
            todoBakeCake.save()

            todoWriteUnitTests.addToTags(tagEasy)
            todoWriteUnitTests.category = categoryJob
            todoWriteUnitTests.save()

        } catch (Exception e) {
            e.printStackTrace()
        }

    }
    def destroy = { }
}
