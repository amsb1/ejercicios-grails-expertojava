package todo

class TodoTagLib {
    static defaultEncodeAs = [taglib:'none']

    static namespace = "todo"

    def printIconFromBoolean = { attrs ->
        def value = attrs['value']
        if(value){
            out <<asset.image(src:"done.png")
        }else{
            out <<asset.image(src:"notdone.png")
        }
    }
}
