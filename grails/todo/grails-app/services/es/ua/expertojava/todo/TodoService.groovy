package es.ua.expertojava.todo

import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.NO_CONTENT

//@Transactional
class TodoService {
    def userService

    def getNext(Integer days, params) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.findAllByDateBetween(now, to, params)
    }

    def countNext(Integer days) {
        Date now = new Date(System.currentTimeMillis())
        Date to = now + days
        Todo.countByDateBetween(now, to)
    }

    def delete(Todo todoInstance) {

        def arrayTagsBorrar = todoInstance?.tags?.findAll()

        arrayTagsBorrar.each {tag ->
            todoInstance.removeFromTags(tag)
        }

        todoInstance.delete(flush:true)

    }

    def listByUser(User usuario,params) {

        Todo.findAllByUser(usuario, [max:params.max])
    }

    def showTodosByUser(String usuario,Integer maxValue) {
        User usuarioAux=User.findByUsername(usuario)
        
        Todo.findByUser(usuarioAux, [max:maxValue])
    }

    def getTodosByCategoryAndUserOrderByDate(List<Category> categorias, User usuario) {
        Todo.findAllByUserAndCategoryInList(usuario,categorias,[sort:"date",order:"desc"])
    }

    def getTodosByCategoryOrderByDate(List<Category> categorias) {
        Todo.findAllByCategoryInList(categorias, [sort:"date",order:"desc"])
    }

    def countTodosByCategory(List<Category> categorias) {
        Todo.countByCategoryInList(categorias)
    }


    def saveTodo(Todo todoInstance) {
        if (todoInstance.done && !todoInstance.dateDone) {
            todoInstance.dateDone = new Date()
        }

        todoInstance.save(flush: true)
    }

    def lastTodosDone(Integer hours) {
        Date endDate = new Date(System.currentTimeMillis())

        Date startDate = new Date(System.currentTimeMillis() - hours*60*60*1000)

        Todo.findAllByDateDoneBetween(endDate, startDate)
    }
}
