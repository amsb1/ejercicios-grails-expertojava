package es.ua.expertojava.todo

import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.NO_CONTENT

//@Transactional
class CategoryService {

   def delete(Category categoryInstance) {

       if (categoryInstance != null) {

           Todo.findAllByCategory(categoryInstance)?.each { cat ->
               cat.category = null
               cat.save()
           }

           categoryInstance.delete(flush: true)
       }
    }
}
