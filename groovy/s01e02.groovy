class Libro {
    String titulo
    int ano
    String autor
    String editorial

    
    String getNombre(){
        return titulo
    }
    int getAnyo(){
        return ano
    }
    String getAutor(){
        def tokens=autor.tokenize(",")
        return tokens[1].trim()+ " "+tokens[0].trim()
    }
}



/* Crea aquí las tres instancias de libro l1, l2 y l3 */
Libro l1 = new Libro(titulo:'La colmena',ano:1951,autor:'Cela Trulock, Camilo José');
Libro l2 = new Libro(titulo:'La galatea',ano:1585,autor:'de Cervantes Saavedra, Miguel');
Libro l3 = new Libro(titulo:'La dorotea',ano:1632,autor:'Lope de Vega y Carpio, Félix Arturo');

assert l1.getNombre() == 'La colmena'
assert l2.getAnyo() == 1585
assert l3.getAutor() == 'Félix Arturo Lope de Vega y Carpio'

/* Añade aquí la asignación de la editorial a todos los libros */
l1.editorial='Anaya';
l2.editorial='Planeta';
l3.editorial= 'Santillana';

assert l1.getEditorial() == 'Anaya'
assert l2.getEditorial() == 'Planeta'
assert l3.getEditorial() == 'Santillana'