class Todo {
    String titulo
    String descripcion
}

def todos = [];

todos +=new Todo(titulo:"Lavadora",descripcion:"Poner lavadora");
todos +=new Todo(titulo:"Impresora",descripcion:"Comprar cartuchos impresora");
todos +=new Todo(titulo:"Películas",descripcion:"Devolver películas videoclub");

todos.each {item -> println "${item.titulo} ${item.descripcion}"}

