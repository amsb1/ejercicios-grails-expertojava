import java.util.Date

// EJERCICIO 2.1.1
/* Añade aquí la implementación del factorial en un closure*/
def factorial={ entero ->
    return (entero<=1)? 1: (call(entero-1)) * entero
}

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */
// EJERCICIO 2.1.2
def lista =[1,2,3,4,5,6,7,8,9,10]

lista.each{item ->
    println "Factorial de $item = "+factorial(item);
}


// EJERCICIO 2.1.3
def ayer={ fecha ->
    Date pop = new Date().parse("dd/MM/yyyy",fecha);

    return pop.previous().format("dd/MM/yyyy");
}

def manana={ fecha ->
    Date pop = new Date().parse("dd/MM/yyyy",fecha);

    return pop.plus(1).format("dd/MM/yyyy");
} 

assert ayer("01/01/2016")=="31/12/2015"
assert manana("01/01/2016")=="02/01/2016"

// EJERCICIO 2.1.4
def listaFechas =["01/01/2015","01/01/2015","01/01/2015","02/01/2015","03/01/2015","04/01/2015","05/01/2015","06/01/2015","07/01/2015","08/01/2015"]
listaFechas {item ->
    println "Anterior $item = "+ayer(item)+"\n"+"Posterior $item = "+posterior(item);
}



