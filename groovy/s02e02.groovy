class Calculadora {

    static float suma(a,b){
        a+b
    }

    static float resta(a,b){
        a-b
    }

    static float mult(a,b){
        a*b
    }

    static String div(a,b){
        if(b!=0) {
            a / b
        }else{
            println ("ERROR: División por 0")
        }
    }
    public static void main(String[] args){
        def ope1
        def ope2
        def op
        def result

        System.in.withReader {
            print 'Introduzca operador: '
            ope1 = it.readLine() as Integer


            print 'Introduzca operador 2: '
            ope2 = it.readLine() as Integer


            print 'Introduzca operacion: '
            op = it.readLine()[0]
        }

        switch(op){
            case '+':
                result=Calculadora.suma(ope1,ope2)
                break;
            case '-':
                result=Calculadora.resta(ope1,ope2)
                break;
            case '*':
                result=Calculadora.mult(ope1,ope2)
                break;
            case '/':
                result=Calculadora.div(ope1,ope2)
                break;
        }

        println(" $ope1 $op $ope2 = "+ result);
    }
}
